
library(ggplot2)
library(ggthemes)
library(igraph)
library(dplyr)

data_input_dir <- '/home/g/gb/gb293/phd/data/networks/input'
data_output_dir <- '/home/g/gb/gb293/phd/data/networks/output'
organism_name = 'human'

## Read interaction data
print('Reading data')
interactions_file = sprintf("%s/%s_interactions.csv", data_output_dir, organism_name)
data <- read.csv(interactions_file, sep = ',', header=T, as.is=T)

data <- data[data$experimental > 800,]
data$gene1 <- gsub('9606.', "", data$gene1)
data$gene2 <- gsub('9606.', "", data$gene2)
data <- data[c('gene1', 'gene2')]
# 
# # Removing Ubiquitin!
# data <- data %>%
#   filter(gene1 != 'ENSG00000150991', gene2 != 'ENSG00000150991')
         
print('Building graph')
nodes <- unique(c(data$gene1,  data$gene2))

g <- graph_from_data_frame(d=data, vertices=nodes, directed=T)
g <- simplify(g)

# get the subgraph correspondent to just the giant component
g.components <- clusters(g)
ix <- which.max(g.components$csize)
g <- induced.subgraph(g, which(g.components$membership == ix))

nodes = as.vector(V(g)$name)

member <- spinglass.community(g, spins=20)$membership
node_m <- data.frame(node=nodes,member)

### Read SL data
sl_file <- sprintf("%s/%s_ssl.csv", data_input_dir, organism_name)
sl <- read.csv(sl_file, header=T, as.is=T)

names(sl) <- c('gene1', 'gene2', 'sym1', 'sym2')

sl <- sl %>%
  filter(gene1 %in% nodes) %>%
  filter(gene2 %in% nodes)

shortest <- shortest.paths(g)
distances <- apply(sl, 1, function(x) {shortest[x['gene1'], x['gene2']]})
sl$shortest_path <- distances

 sl <- merge(x=sl, y=node_m, by.x="gene1", by.y="node")
 sl <- merge(x=sl, y=node_m, by.x="gene2", by.y="node")
 sl <- sl %>%
   mutate(g_interact=paste(member.x, '-', member.y, sep = " "))
 sl_ord <- within(sl,
                    Position <- factor(g_interact,
                                       levels=names(sort(table(g_interact),
                                                         decreasing=TRUE))))
 ggplot(sl_ord) +
   geom_histogram(aes(x=g_interact), stat='count', fill='cornflowerblue', alpha=0.5) +
   theme_bw() +
   xlab('Start and End Community') +
   theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 5))

 
non_sl_sample <- sample(as.vector(shortest), length(sl$shortest_path)*2)

sl_path <- data.frame(path_len=as.vector(sl$shortest_path), sl=1)
all_path <- data.frame(path_len=as.vector(non_sl_sample), sl=0)
path_len_df <- rbind(sl_path, all_path)

sl_path <- 0
all_path <- 0


t_t <-t.test(path_len ~ sl, data=path_len_df)

ggplot(path_len_df) + 
  aes(x=path_len, fill=factor(sl), colour=factor(sl)) +
  geom_density(alpha=0.4, lwd=0.4, adjust=0.5, bw=1) +
  annotate("text", x=8, y=0.475, hjust=1, label=paste("p-value=", format.pval(t_t$p.value))) +
  xlab('Shortest Path Length') +
  theme_bw()



#### Save graph for ploting

V(g)$group = member
write_graph(graph=g, file='human_clustered.gml', format = c("gml"))



##############
# Heavy lifting - - will be good for prediction later but not required now
##############

# node_df <- data.frame(nodes=nodes)
# 
# node_df$member <- member
# 
# expand.grid.df <- function(...) Reduce(function(...) merge(..., by=NULL), list(...))
# exp_df <- expand.grid.df(node_df, node_df)
# 
# exp_df <- exp_df %>%
#   mutate(same=(nodes.x==nodes.y)) %>% 
#   filter(same == FALSE) 



# 
# # This makes things really large!
# exp_df <- exp_df %>%
#   mutate(interaction=paste(nodes.x, '-', nodes.y)) %>%
#   mutate(interaction=paste(member.x, '-', member.y)) %>%
#   
#   
# head(exp_df)
#        
       



