library(igraph)
library(dplyr)

get_graph <- function(organism_name, organism_code, data_dir, gene_list_file) {
  
  #Read ID map
  id_map <- read.table(sprintf('%s/id_maps/%s_id_map.txt', data_dir, organism_name), header=TRUE, sep='\t', fill=T, comment.char = '', quote = '', stringsAsFactors = F)
  names(id_map) <- c('Gene.ID', 'Transcript.ID',	'Protein.ID',	'Associated.Gene.Name',	'External.ID')
  
  ## Read interaction data
  print('Reading and preprocess data')
  interactions <- read.table(sprintf('%s/interactions/%s.protein.links.detailed.v10.txt', data_dir, organism_code), sep = ' ', header=T)
  ## preprocess interaction data
  interactions <- interactions %>% 
    filter(experimental > 800) %>%
    mutate(protein1 = gsub(sprintf('%s.', organism_code), "", protein1)) %>%
    mutate(protein2 = gsub(sprintf('%s.', organism_code), "", protein2)) %>%
    select(c(protein1, protein2))
  
  # Add gene IDs based on protein ID
  gene_protein_map <- id_map %>% select(c(Gene.ID, Protein.ID, Associated.Gene.Name))
  interactions <- merge(interactions, gene_protein_map, by.x='protein1', by.y='Protein.ID', suffixes = c('1', '2'))
  interactions <- merge(interactions, gene_protein_map, by.x='protein2', by.y='Protein.ID', suffixes = c('1', '2'))
  print(head(interactions))
  interactions <- interactions %>% 
    rename(gene1 = Associated.Gene.Name1, gene2 = Associated.Gene.Name2) %>%
    select(c(gene1, gene2))
  rm(gene_protein_map)
  
  # Create subset from gene_list
  gene_list <- unlist(as.vector(read.table(gene_list_file, sep = ' ', header=T)))
  print(gene_list)
  filtered_interactions <- interactions %>%
    filter((gene1 %in% gene_list) & (gene2 %in% gene_list))
  print(head(filtered_interactions))
  
  
  
  # build graph
  print('Building graph')
  nodes <- unique(as.vector(as.matrix(filtered_interactions[,c("gene1", "gene2")])))
  g <- graph_from_data_frame(d=filtered_interactions, vertices=nodes, directed=T)
  g <- simplify(g)
  V(g)$label <- nodes
  rm(filtered_interactions)
  rm(interactions)
  
  return(g)
  
}



data_dir <- '/home/g/gb/gb293/phd/data/slorth'
organism_code_key = c('human'='9606', 'yeast'='4932', 'fly'='7227', 'worm'='6239', 'pombe'='4896')

organism_name = 'human'
organism_code = organism_code_key[organism_name]

gene_list_file <- sprintf('%s/gene_lists/raw/ddr.txt', data_dir)
title <- 'ddr'

graph <- get_graph(organism_name, organism_code, data_dir, gene_list_file)

output_graph_file = sprintf("~/phd/projects/synthetic_lethal/slorth/visualisation/%s_%s_graph.gml",  organism_name, title)
write_graph(graph = graph, file = output_graph_file, format = 'gml')
