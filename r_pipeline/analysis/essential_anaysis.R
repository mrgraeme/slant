library(dplyr)
data_dir = '/its/home/gb293/phd/data/slorth'

library(rjson)
essential <- fromJSON(file=sprintf('%s/essential/essential_genes.json', data_dir))
essential_df <- data.frame('gene'=essential, 'essential'=1)

sym_map <- read.table(sprintf('%s/id_maps/%s_id_map.txt', data_dir, 'human'), header=TRUE, sep='\t', fill=T, comment.char = '', quote = '', stringsAsFactors = F)
sym_map <- sym_map %>% 
  select(Gene.ID, Associated.Gene.Name) %>%
  distinct(Gene.ID, Associated.Gene.Name)

head(sym_map)

essential_df <- essential_df %>%
  left_join(sym_map, by=c('gene'='Associated.Gene.Name')) %>%
  na.omit() %>%
  select(gene=Gene.ID, essential)

head(essential_df)

add_essentials <- function(pairs) {
  
pairs <- pairs %>%
  left_join(essential_df, by=c('gene1'='gene')) %>%
  left_join(essential_df, by=c('gene2'='gene'))

pairs[is.na(pairs)] <- 0

pairs <- pairs %>%
  mutate(one_essential=(essential.x | essential.y)) %>%
  mutate(both_essential=(essential.x  & essential.y))

pairs <- pairs %>%
  select(one_essential, both_essential, sl)

return(pairs)

}

predictions <- read.table(sprintf('%s/results/recent/human_predictions_ssl.csv', data_dir))

predictions <- predictions %>%
  mutate(sl=1)

training <- read.table(sprintf('%s/training/%s_full_features_ssl.csv', data_dir, 'human'), sep='\t', header=TRUE, fill=T, stringsAsFactors = F)  # sl <- features$sl
training <- training %>%
  select(gene1, gene2, sl)
t_sl <- training %>%
  filter(sl==1)
t_non_sl <- training %>%
  filter(sl==0)

t_non_sl_balanced <- sample_n(t_non_sl, nrow(t_sl))

head(training)


tbl <- add_essentials(training)

t_sl <- tbl %>%
  filter(sl==1)
t_non_sl <- tbl %>%
  filter(sl==0)

t_non_sl_balanced <- sample_n(t_non_sl, nrow(t_sl))

head(t_sl)


t_b <- table(select(tbl, both_essential, sl))
t_b
print('Both SL')
t_b[4]/(t_b[3]+t_b[4])

print('Both non-SL')
t_b[2]/(t_b[1]+t_b[2])


t_o <- table(select(tbl, one_essential, sl))
print('One SL')
t_o[4]/(t_o[3]+t_o[4])

print('One non-SL')
t_o[2]/(t_o[1]+t_o[2])






t_p <- add_essentials(predictions)
tbl_p_b <- table(select(t_p, both_essential, sl))
tbl_p_b
print('Both SL pred')
tbl_p_b[2]/(tbl_p_b[1]+tbl_p_b[2])

tbl_p_o <- table(select(t_p, one_essential, sl))
tbl_p_o
print('One SL pred')
tbl_p_o[2]/(tbl_p_o[1]+tbl_p_o[2])






essential <- fromJSON(file=sprintf('%s/essential/essential_genes.json', data_dir))
essential_df <- data.frame('gene'=essential, 'essential'=1)

sym_map <- read.table(sprintf('%s/id_maps/%s_id_map.txt', data_dir, 'human'), header=TRUE, sep='\t', fill=T, comment.char = '', quote = '', stringsAsFactors = F)
sym_map <- sym_map %>% 
  select(Gene.ID, Associated.Gene.Name) %>%
  distinct(Gene.ID, Associated.Gene.Name)

head(sym_map)

essential_df <- essential_df %>%
  left_join(sym_map, by=c('gene'='Associated.Gene.Name')) %>%
  na.omit() %>%
  select(gene=Gene.ID, essential)

head(essential_df)

non_sl_g1 <- training %>%
  filter(sl==0) %>%
  select(x=gene1)

non_sl_g2 <- training %>%
  filter(sl==0) %>%
  select(x=gene2)

non_sl <- rbind(non_sl_g1, non_sl_g2) %>% 
  distinct() %>%
  sample_n(length(sl$x))
non_sl
  



head(non_sl)
sl <- read.csv('human_sl_list.csv', stringsAsFactors = F)
#non_sl <- read.csv('human_non_sl_list.csv', stringsAsFactors = F)
sl
## #sl essential / not essential
es <- length(intersect(essential_df$gene, sl$x))
sprintf('%s/%s', es, length(sl$x)-es)
non_sl <- read.csv('human_non_sl_list.csv')
es <- length(intersect(essential_df$gene, non_sl$x))
sprintf('%s/%s', es, length(non_sl$x)-es)
