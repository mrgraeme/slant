

create_gene_list <- function(organism, flags = F) {
  
distinct_pairs  <- read.table(sprintf('%s/webapp_data/%s_pairs_%s.csv', data_dir, tolower(organism), tag), fill=F, sep='\t', quote='')
print(head(distinct_pairs))

print('Create gene list')
genea <- distinct_pairs %>%
  select(gene=V1, id=V3)
geneb <- distinct_pairs %>%
  select(gene=V2, id=V4)
gene_list <- rbind(genea, geneb)
gene_distinct <- gene_list %>%
  distinct(gene, .keep_all=T)
gene_formatted <- gene_distinct %>%
  mutate(organism=orgs[organism])


if (flags == T) {
print('Add description')

descriptions <- read.csv(sprintf('%s/webapp_data/gene_descriptions.txt', data_dir), sep='\t', header=F) %>%
  select(gene=V3, description_raw=V2) %>%
  group_by(gene) %>%
  distinct() %>%
  summarise(description=paste(description_raw, collapse=', ')) %>%
  as.data.frame() %>%
  distinct()

gene_formatted <- gene_formatted %>%
  left_join(descriptions) %>%
  replace(., is.na(.), '') %>%
  distinct()


print('Add drug flag')
drug_targets <- read.csv(sprintf('%s/webapp_data/drug_data.csv', data_dir), sep='\t', header=F)
targets <- drug_targets %>%
  select(gene = V3) %>%
  mutate(target=1) %>%
  as.data.frame()

gene_formatted <- gene_formatted %>%
  left_join(targets) %>%
  replace(., is.na(.), 0) %>%
  distinct()

print('add disease flag and data')
census <- read.table(sprintf('%s/webapp_data/census.tsv', data_dir), sep='\t', header=T)
census <- census %>%
  mutate(disease=1) %>%
  select(gene=Gene.Symbol, disease)

gene_formatted <- gene_formatted %>%
  left_join(census ) %>%
  replace(., is.na(.), 0) %>%
  distinct()
}


write.table(gene_formatted, sprintf('%s/webapp_data/%s_genes.csv', data_dir, tolower(organism)), sep='\t', quote=F, row.names=F, col.names = F)
return(gene_formatted)
}

h = create_gene_list('Human', T)
head(h)
create_gene_list('Yeast')
create_gene_list('Fly')
create_gene_list('Worm')
create_gene_list('Pombe')







