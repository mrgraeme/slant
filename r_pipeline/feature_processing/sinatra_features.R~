

library(igraph)
library(dplyr)

sort_pairs <- function(sl_pairs) {
  # Order sl pairs alphabetically (useful later on for comparisons)
  sl_pairs = sl_pairs %>% 
    na.omit() %>%
    rowwise() %>% 
    mutate(min_gene=min(gene1, gene2),  max_gene=max(gene1, gene2))
    
  sl_pairs = sl_pairs %>% 
    select(gene1=min_gene, gene2=max_gene) %>%
    distinct() 
  
  return(sl_pairs)
}

process_features <- function(organism_name, organism_code, data_dir) {
  
  print(sprintf('Processing %s', organism_name))
  
  #Read ID map
  id_map <- read.table(sprintf('%s/id_maps/%s_id_map.txt', data_dir, organism_name), header=TRUE, sep='\t', fill=T, comment.char = '', quote = '', stringsAsFactors = F)
  names(id_map) <- c('Gene.ID', 'Transcript.ID',	'Protein.ID',	'Associated.Gene.Name',	'External.ID')
  
  ## Read interaction data
  print('Reading and preprocess data')
  interactions <- read.table(sprintf('%s/interactions/%s.protein.links.detailed.v10.txt', data_dir, organism_code), sep = ' ', header=T, stringsAsFactors = F)
  ## preprocess interaction data
  interactions <- interactions %>% 
            filter(experimental > 800) %>%
            mutate(protein1 = gsub(sprintf('%s.', organism_code), "", protein1)) %>%
            mutate(protein2 = gsub(sprintf('%s.', organism_code), "", protein2)) %>%
            select(c(protein1, protein2))
  
  # Add gene IDs based on protein ID
  gene_protein_map <- id_map %>% select(c(Gene.ID, Protein.ID))
  interactions <- merge(interactions, gene_protein_map, by.x='protein1', by.y='Protein.ID', suffixes = c('1', '2'))
  interactions <- merge(interactions, gene_protein_map, by.x='protein2', by.y='Protein.ID', suffixes = c('1', '2'))
  interactions <- interactions %>% 
                  rename(gene1 = Gene.ID1, gene2 = Gene.ID2) %>%
                  select(c(gene1, gene2))
  rm(gene_protein_map)

  ################ Thin out network #####################
  interactions <- interactions %>%
  	       sample_n(nrow(interactions)*0.9)
  
  # build graph
  print('Building graph')
  nodes <- unique(as.vector(as.matrix(interactions[,c("gene1", "gene2")])))
  print('TP53 in nodes')
  print('ENSG00000141510' %in% nodes)
  g <- graph_from_data_frame(d=interactions, vertices=nodes, directed=T)
  rm(interactions)
  
  
  print('Extract node-wise features')
  features <- data.frame(nodes=nodes)
  features$betweenness <- betweenness(g)
  features$constraint <- constraint(g)
  features$closeness <- closeness(g)
  features$coreness <- coreness(g)
  features$degree <- degree(g)
  features$eccentricity <- eccentricity(g)
  features$eigen_centrality <- eigen_centrality(g)$vector
  features$hub_score <- hub_score(g)$vector
  features$neighborhood1.size <- neighborhood.size(g, 1)
  features$neighborhood2.size <- neighborhood.size(g, 2)
  features$neighborhood5.size <- neighborhood.size(g, 5)
  features$neighborhood6.size <- neighborhood.size(g, 6)
  features$d6neighbours <- features$neighborhood6.size - features$neighborhood5.size
  features$d6_to_d2_neighbours <- features$neighborhood6.size - features$neighborhood2.size
  # Other neighbourhood stuff
  features$page_rank <- page_rank(g)$vector

  

  # Add GO term list for each gene
  print('Adding Go terms')
  go_terms <- read.table(sprintf("%s/go_terms/processed/%s_go_terms.csv", data_dir, organism_name), comment.char = '!', sep=',', header=T, stringsAsFactors = F, fill=T)
  
  features <- features %>%
    left_join(go_terms, by=c('nodes'='Gene.ID'))
  
  # Create pairs data frame and merge node-wise features
  print('Creating pairs dataframe')
  #### 
  #Make proper pairs
  ###
  gene_list <- as.vector(unlist(read.table(sprintf('%s/gene_lists/processed/%s_%s_gene_list.csv', data_dir, organism_name, gi_tag), stringsAsFactors = F)))
  gene_list <- intersect(gene_list, nodes) # gene_list by nodes for full set
  pairs <- as.data.frame(expand.grid(gene_list, gene_list, stringsAsFactors = F)) # swap second gene_list of nodes
  names(pairs) <- c('gene1', 'gene2')
  pairs <- pairs %>% filter(gene1 != gene2)
  pairs <- sort_pairs(pairs)  
  #pairs <- merge(x=pairs, y=features, by.x='gene1', by.y='nodes', suffixes = c(1,2))
  #pairs <- merge(x=pairs, y=features, by.x='gene2', by.y='nodes', suffixes = c(1,2))  ### must must check if these are labelling properl
  
  print('joining node features')
  pairs <- pairs %>%
    left_join(features, by=c('gene1'='nodes'), suffix = c('1','2')) %>%
    left_join(features, by=c('gene2'='nodes'), suffix = c('1','2'))
  
  head(pairs)
  print('ENSG00000141510' %in% pairs$gene1)

  # Label as sl
  print('Labelling as SL')
  sl_file = sprintf("%s/sl_files/processed/%s_%s.csv", data_dir, organism_name, gi_tag)
  sl_pairs <- read.csv(sl_file, stringsAsFactors = F)
  sl_pairs <- sort_pairs(sl_pairs) %>%
    mutate(sl = 1)
  
  sl_pairs
  
  ### Use join to label sl in all pairs
  pairs_l <- pairs[c('gene1', 'gene2')]
  pairs_l <- left_join(pairs_l, sl_pairs, by = c('gene1', 'gene2'))
  # pairs_l %>%
  #   filter(!is.na(sl))
  #print(head(pairs_l))
  pairs_l[is.na(pairs_l)] <- 0  #### This should only be for known negative labels! --- which Sarah will have for me soon!
  pairs$sl <- pairs_l$sl
  
  print('ENSG00000141510' %in% pairs$gene1)
  
  ### Spinwalk ########

  # Process graph for spinwalk
  g_wc <- simplify(g)
  g_wc.components <- clusters(g_wc)
  ix <- which.max(g_wc.components$csize)
  g_wc <- induced.subgraph(g_wc, which(g_wc.components$membership == ix))

  # #### Nodewise spinglass community
  print('Generate walktrap crossing data')
  wc <- cluster_spinglass(g_wc, spins=20)

  cross_cluster <- igraph::crossing(wc, g_wc)
  pair_names <- unlist(lapply(names(cross_cluster), as.character))
  cross=as.vector(cross_cluster)
  print('Get memebership list')
  membership_list = membership(wc)
  member_df <- data.frame(gene=unlist(lapply(names(membership_list), as.character)), community= as.vector(membership_list))

  cross_df <- data.frame(pair=pair_names, cross=cross) %>%
    tidyr::separate(pair, c("gene1", "gene2"), "\\|", fixed=T)
  
  print('add wc features')
  pairs <- pairs %>%
    left_join(cross_df, by=c('gene1', 'gene2')) %>%
    mutate(cross = ifelse(is.na(cross),0,as.numeric(cross))) %>%
    left_join(member_df, by=c('gene1' = 'gene'), suffix=c('1','2')) %>%
    left_join(member_df, by=c('gene2' = 'gene'), suffix=c('1','2')) %>%
    mutate(share_community = (community1 == community2))

  V(g_wc)$community <- membership_list
  E(g_wc)$sl <- pairs_l$sl

  print('ENSG00000141510' %in% pairs$gene1)
  #write_graph(g_wc, sprintf('%s/spinglass/%s_wc_graph.gml', data_dir, organism_name), format = c('gml'))
  
  
  # Calcualte feature distances between pairs
  print('Calcuating feature distance between pairs')
  distance_func <- function(x,y) {
    x <- x + 0.000000001
    y <- y + 0.000000001   ### using to avoid division errors
    diff <- x - y
    avg <- (x + y)/2
    return(diff/avg)
  }

  pairs$betweenness.dist <- distance_func(pairs$betweenness1, pairs$betweenness2)
  pairs$constraint.dist <- distance_func(pairs$constraint1, pairs$constraint2)
  pairs$closeness.dist <- distance_func(pairs$closeness1, pairs$closeness2)
  pairs$coreness.dist <- distance_func(pairs$coreness1, pairs$coreness2)
  pairs$degree.dist <- distance_func(pairs$degree1, pairs$degree2)
  pairs$eccentricity.dist <- distance_func(pairs$eccentricity1, pairs$eccentricity2)
  pairs$eigen_centrality.dist <- distance_func(pairs$eigen_centrality1, pairs$eigen_centrality2)
  pairs$hub_score.dist <- distance_func(pairs$hub_score1, pairs$hub_score2)
  pairs$neighborhood1.size.dist <- distance_func(pairs$neighborhood1.size1, pairs$neighborhood1.size2)
  pairs$neighborhood2.size.dist <- distance_func(pairs$neighborhood2.size1, pairs$neighborhood2.size2)
  pairs$neighborhood5.size.dist <- distance_func(pairs$neighborhood5.size1, pairs$neighborhood5.size2)
  pairs$neighborhood6.size.dist <- distance_func(pairs$neighborhood6.size1, pairs$neighborhood6.size2)
  
  print('ENSG00000141510' %in% pairs$gene1)

  # Extract pairwise features
  # print('Calculating pairwise adhesion')
  # adhesion <- apply(pairs, 1, function(x) {edge_disjoint_paths(g, source = x['gene1'], target = x['gene2'])})
  # pairs$adhesion <- adhesion

  print('Calculating pairwise cohesion')
  cohesion <- apply(pairs, 1, function(x) {vertex_disjoint_paths(g, source = x['gene1'], target = x['gene2'])})
  pairs$cohesion <- cohesion
  
  print('ENSG00000141510' %in% pairs$gene1)

  print('Measuring adjacency')
  adjacent <- apply(pairs, 1, function(x) are_adjacent(g, x['gene1'], x['gene2']))
  pairs$adjacent<- as.numeric(adjacent)
  
  print('ENSG00000141510' %in% pairs$gene1)

  print('Mutual neighbours')
  #### only look aat pairs that have shorest path 2
  mutual_neighbours <- apply(pairs, 1, function(x) { length(intersect(neighborhood(graph=g, order=1, nodes=x['gene1'])[[1]], neighborhood(graph=g, order=1, nodes=x['gene2'])[[1]]))})
  pairs$mutual_neighbours <- mutual_neighbours
  print('Calculating distances')
  shortest <- shortest.paths(g, mode='out')
  distances <- apply(pairs, 1, function(x) {shortest[x['gene1'], x['gene2']]})
  pairs$shortest_path <- distances
  pairs$shortest_path[is.infinite(pairs$shortest_path)] <- 0

  print('ENSG00000141510' %in% pairs$gene1)

  # Calculate shared go terms
  print('Calculating GO terms features')
  shared_go_p <- mapply(function(x, y) paste(intersect(x, y), collapse=", "),
                      strsplit(pairs$go_terms_p1, ", "), strsplit(pairs$go_terms_p2, ", "))
  shared_go_count_p <- mapply(function(x) length(x), strsplit(shared_go_p, ", "))

  pairs$shared_go_p <- shared_go_p
  pairs$shared_go_count_p <- shared_go_count_p

  shared_go_f <- mapply(function(x, y) paste(intersect(x, y), collapse=", "),
                        strsplit(pairs$go_terms_f1, ", "), strsplit(pairs$go_terms_f2, ", "))
  shared_go_count_f <- mapply(function(x) length(x), strsplit(shared_go_f, ", "))

  pairs$shared_go_f <- shared_go_f
  pairs$shared_go_count_f <- shared_go_count_f

  shared_go_c <- mapply(function(x, y) paste(intersect(x, y), collapse=", "),
                        strsplit(pairs$go_terms_c1, ", "), strsplit(pairs$go_terms_c2, ", "))
  shared_go_count_c <- mapply(function(x) length(x), strsplit(shared_go_c, ", "))

  pairs$shared_go_c <- shared_go_c
  pairs$shared_go_count_c <- shared_go_count_c

  print('ENSG00000141510' %in% pairs$gene1)

  return(pairs)

}

data_dir <- '/home/g/gb/gb293/lustre/phd/data'
gi_tag <- 'sl'
string_organism_code_key = c('human'='9606', 'yeast'='4932', 'fly'='7227', 'worm'='6239', 'pombe'='4896')

organism_name = 'human'
string_organism_code = string_organism_code_key[organism_name]
pairs <- process_features(organism_name, string_organism_code, data_dir)
output_features_file = sprintf("%s/training/%s_full_features_%s_90.csv", data_dir, organism_name, gi_tag)
write.table(pairs, file=output_features_file, row.names = F, quote = F, sep='\t')

organism_name = 'yeast'
string_organism_code = string_organism_code_key[organism_name]
pairs <- process_features(organism_name, string_organism_code, data_dir)
head(pairs)
table(pairs$sl)
output_features_file = sprintf("%s/training/%s_filtered_features_%s_90.csv", data_dir, organism_name, gi_tag)
write.table(pairs, file=output_features_file, row.names = F, quote = F, sep='\t')

organism_name = 'worm'
string_organism_code = string_organism_code_key[organism_name]
pairs <- process_features(organism_name, string_organism_code, data_dir)
output_features_file = sprintf("%s/training/%s_full_features_%s_90.csv", data_dir, organism_name, gi_tag)
write.table(pairs, file=output_features_file, row.names = F, quote = F, sep='\t')

organism_name = 'fly'
string_organism_code = string_organism_code_key[organism_name]
pairs <- process_features(organism_name, string_organism_code, data_dir)
output_features_file = sprintf("%s/training/%s_full_features_%s_90.csv", data_dir, organism_name, gi_tag)
write.table(pairs, file=output_features_file, row.names = F, quote = F, sep='\t')

organism_name = 'pombe'
string_organism_code = string_organism_code_key[organism_name]
pairs <- process_features(organism_name, string_organism_code, data_dir)
output_features_file = sprintf("%s/training/%s_filtered_features_%s_90.csv", data_dir, organism_name, gi_tag)
write.table(pairs, file=output_features_file, row.names = F, quote = F, sep='\t')

